#!/usr/bin/env python3

"""Rename files to give them more useful names.

This won't actually touch the original files, it just creates another
directory with simlinks to them. Requires that you have a file that
lists the input DAODs, and a file that lists the outputs from those
jobs.
"""

_help_input_list="list of input DxAODs"
_help_h5_names="list of datasets derived from DxAODs"
_help_dir="directory where downloaded datasets live"
_help_link_dir="directory where links will go (with short names)"

from argparse import ArgumentParser
from pathlib import Path
from os.path import relpath

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_file_names', help=_help_input_list)
    parser.add_argument('h5_dataset_names', nargs='?', help=_help_h5_names)
    parser.add_argument('h5_file_dir', nargs='?', type=Path, help=_help_dir)
    parser.add_argument('-o', '--short-files-dest-dir', type=Path,
                        help=_help_link_dir)
    return parser.parse_args()

CAMPAIGN_MAP = {
    'r9315': 'mc16a',
    'r9364': 'mc16a',
    'r10210': 'mc16d',
    'r10201': 'mc16d',
}

def get_campaign(tags):
    campaign = set(CAMPAIGN_MAP[x] for x in CAMPAIGN_MAP if x in tags)
    if len(campaign) != 1:
        raise LookupError(f'bad bad bad, unknown campaign: {tags}')
    return campaign.pop()

def run():
    args = get_args()
    short_names = []
    with open(args.input_file_names) as input_files:
        for line in input_files:
            pfx, dsid, longname, step, deriv, tags = line.strip().split('.')
            discr = longname.split('_',2)[-1]
            campaign = get_campaign(tags)
            short_names.append('.'.join([campaign, discr, dsid]))

    if not args.h5_dataset_names:
        for short in short_names:
            print(short)
        exit(0)

    with open(args.h5_dataset_names) as ds_names:
        ds_names = [l.strip() for l in ds_names]
    if len(ds_names) != len(short_names):
        raise LookupError('number of short names does not match longer names')
    name_map = {}
    for ds_name, short_name in zip(ds_names, short_names):
        if ds_name.split('.')[3] != short_name.split('.')[2]:
            raise LookupError(f'{ds_name} != {short_name}')
        name_map[ds_name.rstrip('/')] = short_name

    if not args.h5_file_dir:
        for from_ds, to_ds in name_map.items():
            print(f'{from_ds} -> {to_ds}')
        exit(0)

    if args.short_files_dest_dir:
        dest_dir = args.short_files_dest_dir
    else:
        dest_dir = args.h5_file_dir.parent.joinpath('short_names')
    dest_dir.mkdir(parents=True)
    for dataset_path in args.h5_file_dir.glob('user.*'):
        short_name = name_map[dataset_path.name]
        short_path = dest_dir.joinpath(short_name)
        short_path.mkdir(exist_ok=True)
        for data_file in dataset_path.glob('*.h5'):
            link = short_path.joinpath(data_file.name)
            link.symlink_to(relpath(data_file,link.parent))

if __name__ == '__main__':
    run()
