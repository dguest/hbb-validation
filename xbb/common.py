import os, json, re

def get_dsid(fpath):
    assert os.path.isdir(fpath), f'{fpath} is not a directory'
    return int(os.path.basename(fpath).split('.')[2])

PRETTY_MATCHERS = [
    ('rsg_(?P<mgev>.*)'  , r'$m_{{G}} = {mgev}$ GeV'),
    ('zp_(?P<mgev>.*)'   , r"$m_{{Z'}} = {mgev}$ GeV"),
    ('wp_(?P<mgev>.*)'   , r"$m_{{W'}} = {mgev}$ GeV"),
    ('jz_(?P<jzslice>.*)', r'JZW slice {jzslice}'),
]
TERSE_MATCHERS = [
    ('rsg_(?P<mgev>.*)'  , r'${mgev}$'),
    ('zp_(?P<mgev>.*)'   , r"${mgev}$"),
    ('wp_(?P<mgev>.*)'   , r"${mgev}$"),
    ('jz_(?P<jzslice>.*)', r'JZ{jzslice}W'),
]
UGLY_MATCHERS = [
    ('RS_G_hh_.*_M(?P<mgev>[0-9]+)' ,'rsg_{mgev}'  ),
    ('zprime(?P<mgev>[0-9]+)_tt'    ,'zp_{mgev}'   ),
    ('Wprime_WZqqqq_m(?P<mgev>[0-9]+)'    ,'wp_{mgev}'   ),
    ('jetjet_JZ(?P<jzslice>[0-9]+)W','jz_{jzslice}')
]
MATCHERS = {
    'ugly': UGLY_MATCHERS,
    'terse': TERSE_MATCHERS,
    'pretty': PRETTY_MATCHERS
}


def get_name(fpath):
    assert os.path.isdir(fpath), f'{fpath} is not a directory'
    basename = os.path.basename(fpath)
    for exp, template in UGLY_MATCHERS:
        match = re.compile(exp).search(basename)
        if match:
            return template.format(**match.groupdict())

def get_pretty_name(name, matchers=TERSE_MATCHERS):
    try:
        matchers = MATCHERS[matchers]
    except TypeError:
        pass
    for exp, template in matchers:
        match = re.compile(exp).search(name)
        if match:
            return template.format(**match.groupdict())

def is_dijet(dsid, restricted=True):
    if restricted:
        return 361024 <= dsid <= 361028
    return 361020 <= dsid <= 361032 

def is_ditop(dsid, restricted=False):
    return 301322 <= dsid <= 301335

def is_dihiggs(dsid, restricted=False):
    if restricted:
        return 301500 <= dsid <= 301507
    return 301488 <= dsid <= 301507

def is_wz(dsid, restricted=False):
    return 301254 <= dsid <= 301287

def get_denom_dict(denom_file):
    return {int(k): v for k, v in json.load(denom_file).items()}

SELECTORS = {
    'higgs': is_dihiggs,
    'dijet': is_dijet,
    'top': is_ditop,
    'z': is_wz,
}
