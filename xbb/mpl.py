"""
Wrappers for basic matplotlib figures.
"""

import os

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib import gridspec

def helvetify():
    """
    Load 'Helvetica' default font (may be Arial for now)
    """
    from matplotlib import rc
    # 'Comic Sans MS', 'Trebuchet MS' works, Arial works in png...
    rc('font',**{'family':'sans-serif','sans-serif':['Arial']})

def xlabdic():
    return dict(ha='right', x=0.98, fontsize=10)
def ylabdic():
    return dict(ha='right', y=0.98, fontsize=10)

class Canvas:
    default_name = 'test.pdf'
    def __init__(self, out_path=None, figsize=(10,8), ext=None):
        self.fig = Figure(figsize)
        self.canvas = FigureCanvas(self.fig)
        self.ax = self.fig.add_subplot(1,1,1)
        self.ax.set_yscale('log')
        self.out_path = str(out_path)
        self.ext = ext

    def save(self, out_path=None, ext=None):
        output = out_path or self.out_path
        assert output, "an output file name is required"
        out_dir, out_file = os.path.split(output)
        if ext:
            out_file = '{}.{}'.format(out_file, ext.lstrip('.'))
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        self.canvas.print_figure(output, bbox_inches='tight')

    def __enter__(self):
        if not self.out_path:
            self.out_path = self.default_name
        return self
    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        self.save(self.out_path, ext=self.ext)
        return True

class CanvasWithRatio:
    default_name = 'test.pdf'
    def __init__(self, out_path=None, figsize=(10,8), ext=None):
        self.fig = Figure(figsize)
        self.canvas = FigureCanvas(self.fig)
        gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
        self.ax = self.fig.add_subplot(gs[0])
        self.ax.grid(True)
        self.ax.set_yscale('log')
        self.ax2 = self.fig.add_subplot(gs[1])
        self.ax2.grid(True)
        self.ax2.set_yscale('log')

        self.fig.subplots_adjust(hspace=1.0)
        self.out_path = str(out_path)
        self.ext = ext

        # options for the legends
        text_fd = dict(ha='left', va='center')
        atlas_fd = dict(weight='bold', style='italic', size=28, **text_fd)
        internal_fd = dict(size=28, **text_fd)
        info_df = dict(size=18, **text_fd)
        x=0.05
        y=0.94
        shift=0.16
        self.ax.text(x,y,'ATLAS', fontdict=atlas_fd,transform=self.ax.transAxes)
        self.ax.text(x+shift,y, 'Internal', fontdict=internal_fd,transform=self.ax.transAxes)
        self.ax.text(x,y-0.08, r'$\sqrt{s}=13$ TeV', fontdict=info_df,transform=self.ax.transAxes)
        self.ax.text(x,y-0.16, r'$76 < m_{J} < 146$ GeV', fontdict=info_df,transform=self.ax.transAxes)

    def save(self, out_path=None, ext=None):
        output = out_path or self.out_path
        assert output, "an output file name is required"
        out_dir, out_file = os.path.split(output)
        if ext:
            out_file = '{}.{}'.format(out_file, ext.lstrip('.'))
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        self.canvas.print_figure(output, bbox_inches='tight')

    def __enter__(self):
        if not self.out_path:
            self.out_path = self.default_name
        return self
    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        self.save(self.out_path, ext=self.ext)
        return True
