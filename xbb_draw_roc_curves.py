#!/usr/bin/env python3

"""
Draw Roc curves
"""

from argparse import ArgumentParser
from h5py import File
import numpy as np
import json, os
import matplotlib.pyplot as plt

from xbb.mpl import Canvas,CanvasWithRatio
from xbb.style import DISCRIMINANT_NAME_MAP, DISCRIMINANT_COLOR_MAP, DISCRIMINANT_LS_MAP

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('h5_roc_curves')
    parser.add_argument('-d', '--discriminant-list', nargs='*')
    parser.add_argument('-o', '--out-dir', default='plots')
    parser.add_argument('-r', '--ratio', default=None)
    return parser.parse_args()

def run():
    args = get_args()
    discrims = {}
    discrims_top = {}
    with File(args.h5_roc_curves,'r') as h5file:
        for discrim_name, group in h5file.items():
            ds_names = ['higgs', 'dijet', 'top', 'edges']
            if args.discriminant_list:
                if discrim_name not in args.discriminant_list:
                    continue
            roc_components = {
                x: np.asarray(group[x]) for x in ds_names
            }
            discrims[discrim_name] = roc_components
    draw_roc_curves(discrims, args.out_dir)

def draw_roc_curves(discrims, out_dir):
    discrims_higgs = {}
    discrims_top = {}
    for name, disc in discrims.items():
        if 'top_vs_qcd' in name:
            discrims_top[name] = disc
        else:
            discrims_higgs[name] = disc
    draw_roc_curves_multijet(discrims_higgs, out_dir)
    draw_roc_curves_top(discrims_higgs, out_dir)
    draw_top_tagging_roc_curves(discrims_top, out_dir)

def draw_roc_curves_multijet(discrims, out_dir):
    args = get_args() 
    Can = CanvasWithRatio if args.ratio else Canvas
    with Can(f'{out_dir}/roc_multijet.pdf') as can:
        if args.ratio: baseline_sig, baseline_rej = discrims[args.ratio]['higgs'], discrims[args.ratio]['dijet'] 
        else: baseline_sig, baseline_rej = np.array([]),np.array([])
        for dis_name, discrims in discrims.items():
            sig, bg = discrims['higgs'], discrims['dijet']
            draw_roc(can, sig, bg, out_dir, baseline_sig, baseline_rej,
                     label=dis_name)
        can.ax.set_ylabel('Multijet Rejection',fontsize=20)
        can.ax.set_ylim(None,1e4)
        can.ax.legend(frameon=False,fontsize=16)
        if isinstance(can, CanvasWithRatio):
            can.ax2.set_ylabel('Ratio to %s'%args.ratio.upper(),fontsize=20)
            can.ax2.set_xlabel('Higgs Efficiency',fontsize=20)
        else: can.ax.set_xlabel('Higgs Efficiency',fontsize=20)

def draw_roc_curves_top(discrims, out_dir):
    args = get_args() 
    Can = CanvasWithRatio if args.ratio else Canvas
    with Can(f'{out_dir}/roc_top.pdf') as can:
        if args.ratio: baseline_sig, baseline_rej = discrims[args.ratio]['higgs'], discrims[args.ratio]['top'] 
        else: baseline_sig, baseline_rej = np.array([]),np.array([])
        for dis_name, discrims in discrims.items():
            sig, bg = discrims['higgs'], discrims['top']
            draw_roc(can, sig, bg, out_dir, baseline_sig, baseline_rej,
                     label=dis_name)
        can.ax.set_ylabel('Top Rejection',fontsize=20)
        can.ax.set_ylim(None,1e3)
        can.ax.legend(frameon=False,fontsize=16)
        if isinstance(can, CanvasWithRatio):
            can.ax2.set_ylabel('Ratio to %s'%args.ratio.upper(),fontsize=20)
            can.ax2.set_xlabel('Higgs Efficiency',fontsize=20)
        else: can.ax.set_xlabel('Higgs Efficiency',fontsize=20)

def draw_top_tagging_roc_curves(discrims, out_dir):
    args = get_args() 
    Can = CanvasWithRatio if args.ratio else Canvas
    with Can(f'{out_dir}/top_tagging_roc.pdf') as can:
        if args.ratio: baseline_sig, baseline_rej = discrims[args.ratio]['top'], discrims[args.ratio]['dijet'] 
        else: baseline_sig, baseline_rej = np.array([]),np.array([])
        for dis_name, discrims in discrims.items():
            baseline_sig, baseline_rej = np.array([]),np.array([])
            sig, bg = discrims['top'], discrims['dijet']
            draw_roc(can, sig, bg, out_dir, baseline_sig, baseline_rej,
                     label=dis_name)
        can.ax.set_ylabel('Multijet Rejection',fontsize=20)
        can.ax.legend(frameon=False,fontsize=16)
        if isinstance(can, CanvasWithRatio):
            can.ax2.set_ylabel('Ratio to %s'%args.ratio.upper(),fontsize=20)
            can.ax2.set_xlabel('Top Efficiency',fontsize=20)
        else: can.ax.set_xlabel('Top Efficiency',fontsize=20)


def get_disc_name(raw):
    l = raw.lower()
    if raw in DISCRIMINANT_NAME_MAP:
        return DISCRIMINANT_NAME_MAP[raw]
    return raw.upper()

def draw_roc(canvas, sig, bg, out_dir, baseline_sig, baseline_bg, label, min_eff=0.4):
    
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    eff = np.cumsum(sig[1:-1][::-1])[::-1] # numerator only, excluding overflow and underflow
    denom = (np.cumsum(sig[::-1])[::-1]).max() # denominator, including overflow and underflow
    eff /= denom
    bg_eff = np.cumsum(bg[1:-1][::-1])[::-1] # numerator only, excluding overflow and underflow
    bg_denom = np.cumsum(bg[::-1])[::-1].max() # denominator, including overflow and underflow
    bg_eff /= bg_denom

    rej = np.zeros_like(bg_eff)
    valid = bg_eff > 0.0
    rej[valid] = 1/bg_eff[valid]

    xbins = np.arange(sig[1:-1].size)

    valid_eff = eff > min_eff
    fullname = get_disc_name(label)
    lines = canvas.ax.plot(eff[valid_eff], rej[valid_eff], label=fullname,
                  color=DISCRIMINANT_COLOR_MAP[label],linestyle=DISCRIMINANT_LS_MAP[label],linewidth=2)
    
    if isinstance(canvas, CanvasWithRatio):

        baseline_eff, baseline_rej = get_baseline_rejection(baseline_sig,baseline_bg,min_eff)
        this_rej = lines[0].get_ydata()
        this_eff = lines[0].get_xdata()

        from scipy import interpolate
        f = interpolate.interp1d(baseline_eff, baseline_rej, bounds_error=False, fill_value=baseline_rej[0])
        interp_baseline_rej = f(this_eff)
        
        # define ratio to plot
        ratio_of_rejections = np.ones_like(interp_baseline_rej)
        
        ratio_of_rejections = this_rej/interp_baseline_rej
        canvas.ax2.plot(this_eff,ratio_of_rejections,label=fullname,
                   color=DISCRIMINANT_COLOR_MAP[label],linestyle=DISCRIMINANT_LS_MAP[label],linewidth=2)
        
    canvas.fig.tight_layout()

    with Canvas(f'{out_dir}/{label}.pdf') as can:
        can.ax.step(xbins, eff, label='signal')
        can.ax.step(xbins, bg_eff, label='bg')
        can.ax.legend()

def get_baseline_rejection(sig,bg,min_eff):

    eff = np.cumsum(sig[1:-1][::-1])[::-1] # numerator only, excluding overflow and underflow
    denom = (np.cumsum(sig[::-1])[::-1]).max() # denominator, including overflow and underflow
    eff /= denom
    bg_eff = np.cumsum(bg[1:-1][::-1])[::-1] # numerator only, excluding overflow and underflow
    bg_denom = np.cumsum(bg[::-1])[::-1].max() # denominator, including overflow and underflow
    bg_eff /= bg_denom

    rej = np.zeros_like(bg_eff)
    valid = bg_eff > 0.0
    rej[valid] = 1/bg_eff[valid]

    valid_eff = eff > min_eff
    lines = plt.plot(eff[valid_eff], rej[valid_eff])
    
    return lines[0].get_xdata(),lines[0].get_ydata()

if __name__ == '__main__':
    run()
