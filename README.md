Hbb Training Dataset Validation Scripts
=======================================

This repository contains several scripts to verify that the training
datsets for H->bb tagging are doing what we think they should be
doing. Since several stages are required to properly assign weights to
the samples, it's important that a few scripts be run first.

 - To calculate the right normalization for the dijet samples, you
   have to run `get_weight_denominator.py`. Note that this _only_
   needs to be run over the dijet samples. This will produce a
   "denominator" json file that the other scripts require.

 - To calculate the right normalization for the higgs samples, you
   need to run `make_jet_pt_hist.py`. This will produce a few files in
   `pt-hists` which are used to scale the higgs jet pt spectrum to
   match the dijet spectrum.

 - After these steps are run you can run `make_roc_curves.py` which
   will plot a few roc curves for the baseline H->bb tagging
   discriminants.


Step-by-step instructions
===========================
Using as an example the mc16a xbb training samples listed in [xbb-datasets](https://gitlab.cern.ch/atlas-boosted-hbb/xbb-datasets/).

1. Create symlinks with shorter and user-friendly names
```
python rename.py xbb-datasets/p3990/mc16a-input-datasets.txt xbb-datasets/p3990/mc16a-hbb-datasets.txt <sample_location> -o <folder_renamed_samples>
```

2. Determine normalization for dijet samples:
```
python xbb_get_weight_demominator.py -o <folder_renamed_samples>/denom.json <folder_renamed_samples>/mc16a.*
```

3. For sample pt reweighting:
```
python xbb_make_jet_pt_hist.py -d <folder_renamed_samples>/denom.json -x data/xsec.txt <folder_renamed_samples>/mc16a.* -o <pt_hists>
```

4. Make roc curve (define discriminator/tagger in `xbb_make_roc_curves.py`):
```
python xbb_make_roc_curves.py -i <pt_hists> -x data/xsec.txt -d <folder_renamed_samples>/denom.json -s <h5_roc_tagger>.h5 <folder_renamed_samples>/mc16a.*
```

5. (optional) Combine roc curves of all discriminators into single h5 file:
```
h5copy -i <h5_roc_tagger>.h5 -o roc_combined.h5 -s <tagger> -d <tagger>
```

6. Draw roc curves with ratio panel (discriminator in denominator determined by `-r`):
```
python xbb_draw_roc_curves.py roc_combined.h5 -d <discrim1> <discrim2> -r <discrim1>
```
