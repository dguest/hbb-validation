#!/usr/bin/env python3

"""
Draw roc 2d roc curves in pt vs eff space
"""

from argparse import ArgumentParser
from glob import glob
import os
from pathlib import Path

import numpy as np
from h5py import File
from scipy.interpolate import LinearNDInterpolator
from scipy.spatial import Delaunay

from xbb.mpl import Canvas, helvetify, xlabdic, ylabdic
from xbb.style import DISCRIMINANT_NAME_MAP, PROCESS_NAME_MAP
from matplotlib.colors import LogNorm, Normalize

from xbb_make_roc_curves import DISCRIMINANT_GETTERS, DISCRIMINANT_EDGES

PLOTDIR = 'roc2d'

def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('roc_file', type=Path)
    parser.add_argument('-o', '--out-dir', type=Path, default='plots')
    parser.add_argument('-v', '--verbose', action='store_true')
    return parser.parse_args()


def integrate(hist):
    hist = np.cumsum(hist[::-1,:], axis=0)[::-1,:]
    hist = np.cumsum(hist[:,::-1], axis=1)[:,::-1]
    return hist

def draw_2d_hist(hist, out_dir, file_name, discrim_name, limits,
                 do_log=False, vrange=None,
                 cb_label='Background Rejection Ratio vs DL1'):

    # make the pt into TeV
    extent = (*limits[0], *(l * 1e-6 for l in limits[1]))

    args = dict(
        aspect='auto',
        origin='lower',
        extent=extent,
        interpolation='nearest',
        norm=LogNorm() if do_log else Normalize())
    if vrange:
        args['vmin'], args['vmax'] = vrange

    out_dir.mkdir(parents=True, exist_ok=True)

    with Canvas(out_dir.joinpath(file_name)) as can:
        im = can.ax.imshow(hist.T, **args)
        con = can.ax.contour(hist.T, [1], colors='darkblue', origin='lower',
                             extent=extent)
        cb = can.fig.colorbar(im)
        cb.add_lines(con)
        can.ax.set_xlabel('Higgs Efficiency', **xlabdic())
        can.ax.set_ylabel(r'Minimum Jet $p_{\rm T}$ [TeV]', **ylabdic())
        cb.set_label(cb_label)

def get_roc_interp(name, discrim, limits, signal='higgs', background='dijet',
                   y_inflation=2.0, diagnostics=False,
                   out_dir=Path('test')):
    eff = integrate(discrim[signal])
    bg_eff = integrate(discrim[background])

    efmax = eff[1:,:].max(axis=0)
    v = efmax != 0
    eff[:,v] /= efmax[v]
    if diagnostics:
        draw_2d_hist(eff[1:,:], '.', 'effhist.pdf', discrim)

    efmax = bg_eff[1:,:].max(axis=0)
    v = efmax != 0
    bg_eff[:,v] /= efmax[v]

    # there N bins, -2 for overflow, +1 because of fenceposting
    pt = np.linspace(*limits[1], eff.shape[1] - 1)
    # drop the underflow bins
    x, z = eff[1:,1:], bg_eff[1:,1:]
    y = np.meshgrid(x[:,0],pt)[1].swapaxes(0,1)
    if diagnostics:
        with Canvas(f'roc_{name}.pdf') as test:
            for band in [0, 1, 2]:
                test.ax.plot(x[:,band], 1 / z[:,band], label=f'{band}')
            test.ax.set_yscale('log')
            test.ax.legend()
    # build x, y values to interpolate over
    x_vs_y = np.stack((x, y), 2)
    flat_xy = x_vs_y.reshape(-1,2)
    yscale = flat_xy[:,1].ptp() * y_inflation
    flat_xy[:,1] /= yscale
    grid = Delaunay(flat_xy)
    if diagnostics:
        with Canvas(f'points_{name}.pdf') as can:
            flatx, flaty = flat_xy[:,0], flat_xy[:,1]
            can.ax.scatter(flatx, flaty, c=np.log(z.flatten()) )
            can.ax.triplot(flatx, flaty, grid.simplices.copy())
    interp = LinearNDInterpolator(grid, z.flatten())
    xint, yint = np.linspace(0.4,1,501), np.linspace(pt[0], pt[-1], 501)
    xi = np.stack(np.meshgrid(xint, yint), 2).swapaxes(0,1)
    xi_scale = np.array(xi)
    xi_scale[:,:,1] /= yscale
    zi = interp(xi_scale)
    if diagnostics:
        with Canvas(f'interp_{name}.pdf') as test:
            for pt in range(0, len(pt), len(pt) // 5):
                xs = xi[:, pt ,0]
                zs = zi[:, pt]
                v = ~np.isnan(zs)
                test.ax.scatter(xs[v], zs[v].max() / zs[v], label=f'{pt}')
            test.ax.legend()
            test.ax.set_yscale('log')
    label = r'$1 / \epsilon_{{\rm bg}}$ for {}'.format(
        DISCRIMINANT_NAME_MAP[name])
    draw_lims = [(l.min(), l.max()) for l in [xint, yint]]
    draw_2d_hist(zi,out_dir=out_dir.joinpath(PLOTDIR).joinpath(background),
                 limits=draw_lims,
                 file_name=f'{name}.pdf', cb_label=label,
                 discrim_name=name, do_log=True)
    return zi, draw_lims


def run():
    args = get_args()
    discrims = {}
    limits = None
    helvetify()
    with File(args.roc_file, 'r') as rocs:
        for dname, discrim in rocs.items():
            discrims[dname] = {x: np.asarray(y) for x, y in discrim.items()}
            lims = [x.attrs['limits'] for x in discrim.values()]
            assert all((x == lims[0]).all() for x in lims)
            limits = lims[0]

    draw_2d_rocs(discrims, args.out_dir, limits)

def draw_2d_rocs(discrims, out_dir, limits, backgrounds=['dijet','top']):
    cache = Path('cache')
    if cache.exists():
        print(f'{cache} found, delete to rebuild')
    cache.mkdir(exist_ok=True)

    rocs = {}
    for name, discrim in discrims.items():
        for bg in backgrounds:
            cachedir = cache.joinpath(name, bg)
            cacheobj = cachedir.joinpath('roc.h5')
            if cachedir.exists():
                print(f'loading {name}, higgs / {bg}')
                with File(cacheobj) as h5file:
                    rocs[(name,bg)] = np.asarray(h5file['rocs'])
                    lims = h5file['rocs'].attrs['lims']
            else:
                print(f'drawing {name}, higgs / {bg}')
                cachedir.mkdir(parents=True)
                rocs[(name, bg)], lims = get_roc_interp(
                    name, discrim, limits, out_dir=out_dir,
                    background=bg)
                with File(cacheobj,'w') as h5file:
                    ds = h5file.create_dataset('rocs', data=rocs[(name,bg)])
                    ds.attrs['lims'] = lims

    for bg in backgrounds:
        proc_name = PROCESS_NAME_MAP[bg]
        cb_label = f'{proc_name} Rejection: XbbScore / DL1'
        draw_2d_hist(rocs[('dl1',bg)]/rocs[('xbb_mixed',bg)], limits=lims,
                     out_dir=out_dir.joinpath(PLOTDIR),
                     file_name=f'{bg}-rej-ratio.pdf',
                     discrim_name=name, #vrange=(0.5, 200),
                     do_log=False, cb_label=cb_label)


if __name__ == '__main__':
    run()
